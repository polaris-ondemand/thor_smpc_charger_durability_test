# README #

### What is this repository for? ###

* Controlling the SMPC Battery Charger used in Polaris Thor (Electric Ranger (Kinetic)) off-road vehicle that is built in partnership with Zero Motorcycles.
* This repository supports creating a single executable (.exe) file of the main program
  * use the 'create_executable.py' to create this

### Executable Versioning ###
* Andrew and I's gameplan for this is that once a .exe is made of the program, that version gets a git 'tag' assigned to it
* That tag will mean that commit should now be frozen in time, and that commit will have the .exe uploaded into the repository

### How do I get set up? ###

* Summary of set up
  * must use an IXXAT
    * must know the unique hardware identifier of that IXXAT

### Contribution guidelines ###

* Continue to use PEP8 Guidelines

### Who do I talk to? ###

* Repo Creator: Calvin Henderson
* Electric Powertrain Validation Team: Steven Hessel; Michael Kissler