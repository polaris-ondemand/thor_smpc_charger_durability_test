import PyInstaller.__main__
import SMPC_Charger_Durability_Test_GUI

# support variables to append version to executable file name
ver = SMPC_Charger_Durability_Test_GUI.__version__
main_script_filename = 'SMPC_Charger_Durability_Test_GUI.py'
main_script_name = main_script_filename.replace('.py', '')
save_name = f'{main_script_name}_v{ver}'
save_name_opt = f'--name={save_name}'

PyInstaller.__main__.run([
    main_script_filename,
    '--onefile',
    '--add-data=./SMPC_3kW_SlightEdits.eds;./',
    save_name_opt
])
