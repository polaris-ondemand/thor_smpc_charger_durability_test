import logging
import os
import sys
import canopen
import time
import threading


class SmpcChargerControl:
    def __init__(self, ixxat_hw_id):
        logging.debug('init')

        self.__network = canopen.Network()
        logging.debug('network created')
        self.__network.connect(UniqueHardwareId=ixxat_hw_id, bustype='ixxat', channel=0, bitrate=500000)

        self.charger_stats = []  # list of all the chargers found with their stats

        self.pdo_transmit_thread = threading.Thread(target=self.__transmit_pdos, daemon=True)

    def __add_smpccharger_node_fastscan(self):
        node_id_range_start = 0x10
        # find devices on the CAN bus which have not been assigned a Node ID
        print(" +++ Attempting to find devices using Fastscan +++")
        device_with_no_node_id_found = True
        # loop until all devices have been assigned Node IDs
        while device_with_no_node_id_found:
            print(" -Fastscan iteration starting")
            device_with_no_node_id_found, lss_identities = self.__network.lss.fast_scan()
            if device_with_no_node_id_found:
                print(f'  -Device Found!')
                print(f'    Vendor ID: {lss_identities[0]}')
                print(f'    Product Code: {lss_identities[1]}')
                print(f'    Revision Number: {lss_identities[2]}')
                print(f'    Serial Number: {lss_identities[3]}')
                # assign the found device a NodeID, note that no "switch_state" is needed when using fastscan because once a device is discovered using FastScan it is automatically ready to accept a NodeID assignment (see CiA 305 documentation)
                self.__network.lss.configure_node_id(node_id_range_start)
                self.__network.lss.send_switch_state_global(self.__network.lss.WAITING_STATE)
                # put the found device onto the NETWORK
                node_to_add = self.__network.add_node(node_id_range_start, "SMPC_3kW_SlightEdits.eds")
                logging.info(f'  -Added to Network the Node: {node_to_add}')
                node_id_range_start = node_id_range_start + 1
            else:
                print("  -An iteration of Fastscan was attempted and found no more devices on the CAN bus")

    def __assign_charger_pdos(self):
        try:
            for node_id in self.__network:
                node = self.__network[node_id]
                node.rpdo.read()
                # assign data to the RPDOs
                # RPDO 1
                node.rpdo[1]['PDO0 Received.Destination Node ID'].phys = node_id
                node.rpdo[1]['PDO0 Received.Control Bits'].phys = 34  # control bits are documented somewhere else, though I wish they were in the object dictionary
                node.rpdo[1]['PDO0 Received.Pack Voltage x1000'].phys = 117200  # units of millivolts
                node.rpdo[1]['PDO0 Received.ID_Serial'].phys  # FIXME

                # RPDO 2
                node.rpdo[2]['PDO1 Received.Destination Node ID'].phys = node_id
                node.rpdo[2]['PDO1 Received.Max Voltage x1000'].phys = 120000  # units of millivolts
                node.rpdo[2]['PDO1 Received.Max Current x1000'].phys = 23000
                node.rpdo[2]['PDO1 Received.Max_AC_Current_x1_EVSE_Override'].phys = 17  # no idea why this needs to be 17 but it does

                # RPDO 3
                node.rpdo[3]['PDO2 Received.Destination Node ID'].phys = node_id
                node.rpdo[3]['PDO2 Received.MBB Time'].phys = 1650662616

        except:  # except when a variable is passed in that is not a valid SMPC charger network node
            logging.error("Make sure to pass in only SMPC Charger Network Nodes")

    def __transmit_pdos(self):
        logging.debug('transmit pdos')
        while True:
            for node_id in self.__network:
                node = self.__network[node_id]
                # update the charger's time RPDO
                epoch_seconds = int(time.time())
                node.rpdo[3]['PDO2 Received.MBB Time'].phys = epoch_seconds
                # transmit the PDOs a single time during this loop iterations, swapping the Destination ID each time
                for each_rpdo_map_number in node.rpdo:
                    node.rpdo[each_rpdo_map_number].transmit()
                self.__gather_stats()
                time.sleep(0.05)

    def __stop_pdos(self):
        # stop all rpdos
        for node_id in self.__network:
            node = self.__network[node_id]
            for eachRpdoMapNumber in node.rpdo:
                node.rpdo[eachRpdoMapNumber].stop

    def __search_for_and_add_existing_chargers(self):
        charges_added = False
        logging.debug(' +++ Searching For Nodes on the CAN Bus +++')
        # This will attempt to read an SDO from nodes 1 - 127
        self.__network.scanner.search()
        # We may need to wait a short while here to allow all nodes to respond
        time.sleep(0.05)
        for node_id in self.__network.scanner.nodes:
            time.sleep(0.05)
            logging.debug(f' -Found node {hex(node_id)}, {node_id}(dec)!')

            node = self.__network.add_node(node_id, None)

            if self.__node_is_charger(node):
                logging.debug('node is charger')
                # put the found cahrger onto the network
                eds_file_location = self.__resource_path("SMPC_3kW_SlightEdits.eds")
                self.__network.add_node(node_id, eds_file_location)
                charges_added = True
            else:
                logging.error('Found Nodes That Are Not Chargers on the CAN bus')
                logging.debug('node NOT charger')

        return charges_added

    def __node_is_charger(self, node):
        node_is_charger = False
        SMPC_CHARGER_VENDOR_ID = 0x466
        SMPC_CHARGER_PRODUCT_CODE = 0x9

        time.sleep(0.1)
        node_vendor_id = int.from_bytes(node.sdo.upload(0x1018, 1), 'little')
        time.sleep(0.1)
        node_product_code = int.from_bytes(node.sdo.upload(0x1018, 2), 'little')
        time.sleep(0.1)
        node_is_charger = ((SMPC_CHARGER_VENDOR_ID == node_vendor_id)
                           & (SMPC_CHARGER_PRODUCT_CODE == node_product_code))
        return node_is_charger

    def __assign_stats_array(self):
        for node_id in self.__network:
            self.charger_stats.append(ChargerNode(node_id))

    def __gather_stats(self):
        for node_id in self.__network:
            node = self.__network[node_id]
            for i in range(len(self.charger_stats)):
                if self.charger_stats[i].name == node_id:
                    index = i
                    break
            node.tpdo.read()  # need to read to pull first set of information into Python
            time.sleep(0.1)  # allow time for the tpdo read to update, I think needs to be at least as long as the sync

            # build the chargers' statistic
            current_charger = self.charger_stats[index]
            current_charger.stats['serial_number'] = node.tpdo[4]["PDO3 Transmit.Serial Number"].phys
            current_charger.stats['in_voltage'] = node.tpdo[3]['PDO2 Transmit.AC Voltage x1'].phys
            current_charger.stats['in_current'] = node.tpdo[3]['PDO2 Transmit.AC Current x1000'].phys / 1000
            current_charger.stats['in_frequency'] = node.tpdo[3]['PDO2 Transmit.AC Frequency Hz'].phys
            current_charger.stats['temperature'] = node.tpdo[3]['PDO2 Transmit.Max Temperature Plus 50C'].phys - 50
            current_charger.stats['out_current'] = node.tpdo[1]['PDO0 Transmit.Charge Current'].phys / 1000
            current_charger.stats['out_voltage'] = node.tpdo[1]['PDO0 Transmit.Charge Voltage'].phys / 1000
            current_charger.stats['fan1speed'] = node.sdo['SMPC Fan Status']['Fan 1 Speed'].phys
            current_charger.stats['fan2speed'] = node.sdo['SMPC Fan Status']['Fan 2 Speed'].phys

    def start(self):
        self.__network.sync.start(0.1)
        if not self.__search_for_and_add_existing_chargers():
            self.__add_smpccharger_node_fastscan()
        self.__assign_charger_pdos()
        self.__assign_stats_array()
        self.pdo_transmit_thread.start()
        logging.debug('starting')

    def stop(self):
        self.__stop_pdos()
        self.__network.disconnect()

    def get_charger_stats(self):
        return self.charger_stats

    def __resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
        return os.path.join(base_path, relative_path)


class ChargerNode:
    def __init__(self, node_id):
        self.name = node_id
        self.stats = {
            'node_id': node_id,
            'serial_number': -1,
            'in_voltage': -1,
            'in_current': -1,
            'in_frequency': -1,
            'temperature': -1,
            'fan1speed': -1,
            'fan2speed': -1,
            'out_voltage': -1,
            'out_current': -1,
            }

    def print_keys(self):
        for key, value in self.stats.items():
            print(f'{key} -> {value}')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    try:
        charger_control = SmpcChargerControl("HW616027")
        charger_control.start()
        # logging.INFO('got past start using threads')

        print('print whatever 1')
        time.sleep(5)
        print('print whatever 2')
    except KeyboardInterrupt:
        logging.warning("CTRL + C pressed. Program interrupted")
    finally:
        print('print whatever')
        logging.INFO('exited the main task')
