import logging
from smpc_charger_control import smpc_charger_control as control
import tkinter as tk


class SmpcChargerControlGui:
    def __init__(self):
        logging.debug('initializing the class')
        self.root = tk.Tk()

    # for building the GUI
    def __build(self):
        self.root.title('Thor SMPC Charger Control GUI')
        self.root.welcome_lbl = tk.Label(text='Welcome to the Thor SMPC Charger Control GUI!')
        self.root.ixxat_serial_lbl = tk.Label(text='ixxxat serial for control:')
        self.root.ixxat_serial_inpt = tk.Entry()
        self.root.ixxat_serial_inpt.insert(0, 'HW573520')  # add default text to the entry box
        self.root.ixxat_serial_btn = tk.Button(text='1. Send Commands to Chargers', command=self.__start_charger_control)
        self.root.monitor_btn = tk.Button(text='Charger Status Monitor', command=self.__stats_populate)
        self.root.monitor_btn['state'] = 'disable'

        self.root.welcome_lbl.grid(row=0, column=1)
        self.root.ixxat_serial_lbl.grid(row=1, column=0)
        self.root.ixxat_serial_inpt.grid(row=1, column=1)
        self.root.ixxat_serial_btn.grid(row=1, column=2)
        self.root.monitor_btn.grid(row=3, column=0)

        # add tooltip hints for the user
        CreateToolTip(self.root.monitor_btn, 'Can only be clicked after sending commands to chargers'
                                             '\nClick this button to view charger statistics'
                                             '\nStats update for 25 seconds, then stop to conserve processing power'
                                             '\nClick this button again to see stats for another 25 seconds')
        CreateToolTip(self.root.ixxat_serial_btn, 'Click this button first to start the test'
                                                  '\nThe process may take a few minutes to complete'
                                                  '\nWhen this process is done, the "monitor" button will be active')
        CreateToolTip(self.root.ixxat_serial_inpt, 'Enter the Unique Hardware ID of the IXXAT being used for this test'
                                                   '\nThis alphanumeric is printed on the IXXAT device itself')

    def start_gui(self):
        self.__build()
        self.root.mainloop()

    def __start_charger_control(self):
        ixxat_serial_user_input = self.root.ixxat_serial_inpt.get()
        self.charger_control = control.SmpcChargerControl(ixxat_serial_user_input)
        self.charger_control.start()
        self.charger_stats = self.charger_control.get_charger_stats()
        self.root.ixxat_serial_btn['state'] = 'disable'
        self.root.monitor_btn['state'] = 'normal'

    def __stats_populate(self):
        self.stats_rows = []
        self.stats_rows_stringvars = []
        row = 5
        # build the array
        for charger in self.charger_stats:
            col = 0
            stats_cols = []
            stats_cols_stringvars = []
            for key in charger.stats.items():
                stats_cols_stringvars.append(tk.StringVar())
                stats_cols.append(tk.Label(self.root, textvariable=stats_cols_stringvars[col]))
                col += 1
            self.stats_rows.append(stats_cols)
            self.stats_rows_stringvars.append(stats_cols_stringvars)

        # build header in window
        charger = self.charger_stats[0]
        col = 0
        for key, value in charger.stats.items():
            tk.Label(self.root, text=key).grid(row=row, column=col)
            col += 1

        # place array into the window
        for charger in self.stats_rows:
            row += 1
            col = 0
            for stat in charger:
                stat.grid(row=row, column=col)
                col += 1

        self.__stats_update()
        self.root.after(25000, self.__stats_populate_stop)

    def __stats_update(self):
        row = 0
        for charger in self.charger_stats:
            col = 0
            for key, value in charger.stats.items():
                the_row = self.stats_rows_stringvars[row]
                var = the_row[col]
                var.set(value)
                col += 1
            row += 1
        self.stats_updater = self.root.after(200, self.__stats_update)

    def __stats_populate_stop(self):
        self.root.after_cancel(self.stats_updater)
        row = 0
        for charger in self.charger_stats:
            col = 0
            for key, value in charger.stats.items():
                the_row = self.stats_rows_stringvars[row]
                var = the_row[col]
                var.set('-1')
                col += 1
            row += 1


class CreateToolTip(object):
    """
    create a tooltip for a given widget
    """

    def __init__(self, widget, text='widget info'):
        self.waittime = 500  # miliseconds
        self.wraplength = 180  # pixels
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.widget.bind("<ButtonPress>", self.leave)
        self.id = None
        self.tw = None
        self.add_tooltip_icon()

    # changes the text within the button to show there is a tooltip available
    def add_tooltip_icon(self):
        self.widget['text'] = self.widget.cget('text') + ' (?)'

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.showtip)

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def showtip(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw,
                         text=self.text,
                         justify='left',
                         background="#ffffff",
                         relief='solid',
                         borderwidth=1,
                         wraplength=self.wraplength)
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tw
        self.tw = None
        if tw:
            tw.destroy()
