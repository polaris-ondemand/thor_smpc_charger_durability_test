from smpc_charger_control import smpc_charger_control as scc
from smpc_charger_control import smpc_charger_control_gui as gui
import logging
import can.interfaces.ixxat  # this line allows Pyinstaller to compile the .exe correctly

__version__ = '0.0.2'

if __name__ == '__main__':
    #logging.basicConfig(level=logging.DEBUG)

    logging.debug('name was main')

    charger_gui = gui.SmpcChargerControlGui()

    charger_gui.start_gui()

